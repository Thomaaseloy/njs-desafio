var express = require('express');
var routes = express.Router();

var controllerTopArtist = require('./controller/top_artist');
var controllerTopTrackForArtistNumberOne = require('./controller/top_track_for_artist_number_one');
var controllerPerfilArtistNumberOne = require('./controller/perfil_artist_number_one');

routes.get('/top_artist', async function(req, res) {
    try{
        var response = await controllerTopArtist.topArtist(req,res);
        res.statusCode = response.statusCode;
        res.send(response.data);
    }catch(e){
        console.error(e);
    }
});

routes.get('/top_track_for_artist_number_one', async function(req, res) {
    try{
        var response = await controllerTopTrackForArtistNumberOne.topTrackForArtistNumberOne(req,res);
        res.statusCode = response.statusCode;
        res.send(response.data);
    }catch(e){
        console.error(e);
    }
});

routes.get('/perfil_artist_number_one', async function(req, res) {
    try{
        var response = await controllerPerfilArtistNumberOne.perfilArtistNumberOne(req,res);
        res.statusCode = response.statusCode;
        res.send(response.data);
    }catch(e){
        console.error(e);
    }
});

module.exports = routes;