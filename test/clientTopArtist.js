const
    expect = require('chai').expect,
    proxyquire = require('proxyquire'),
    Response = require('mock-express-response');

let req, res;

describe('Escenario TopTenArtists StatusCode 200', async () => {
    beforeEach(async () => {
        req = {};
        res = new Response();
    });

    it('StatusCode 200: Cuando despliega correctamente el Top Ten de los artistas.', async () => {
        await getService().topArtist(req, res);
        expect(res.statusCode).to.be.equal(200);
    });

    let getService = () => {
        return proxyquire('../controller/top_artist', {});
    };
});

describe('Escenario TopTenArtists StatusCode 500', async () => {
    beforeEach(async () => {
        req = {};
        res = new Response();
    });

    it('StatusCode 500: Cuando se produce un error en la respuesta del servicio.', async () => {
        await getService().topArtist(req, res);
        expect(res.statusCode).to.be.equal(500);
    });

    let getService = () => {
        return proxyquire('../controller/top_artist', {
            '../client/clientTopArtist': {
                clientTopArtist(req, res){
                    process.nextTick(function () {
                        return '';
                    })
                }
            },
            'config': {
                config_server: {
                    address: 'http://localhost:80',
                    profile: 'local'
                }

            }
        });
    };
});