const
    expect = require('chai').expect,
    proxyquire = require('proxyquire'),
    Response = require('mock-express-response');

let req, res;

describe('Escenario ClientePerfilTrack StatusCode 200', async () => {
    beforeEach(async () => {
        req = {
            headers:{
                mbid:'b49b81cc-d5b7-4bdd-aadb-385df8de69a6'
            }
        };
        res = new Response();
    });

    it('StatusCode 200: Cuando encuentra un artista con el mbid ingresado.', async () => {
        await getService().perfilArtistNumberOne(req, res);
        expect(res.statusCode).to.be.equal(200);
    });

    let getService = () => {
        return proxyquire('../controller/perfil_artist_number_one', {});
    };
});

describe('Escenario ClientePerfilTrack StatusCode 200', async () => {
    beforeEach(async () => {
        req = {
            headers:{
                mbid:'f4fdbb4c-e4b7-47a0-b83b-d91bbfcfa387'
            }
        };
        res = new Response();
    });

    it('StatusCode 200: Cuando encuentra un artista con el mbid ingresado pero no tiene canciones.', async () => {
        await getService().perfilArtistNumberOne(req, res);
        expect(res.statusCode).to.be.equal(200);
    });

    let getService = () => {
        return proxyquire('../controller/perfil_artist_number_one', {});
    };
});

describe('Escenario ClientePerfilTrack StatusCode 300', async () => {
    beforeEach(async () => {
        req = {
            headers:{
                mbid:'b49b81cc-d5b7-4bdd-aadb-385df8de69a7'
            }
        };
        res = new Response();
    });

    it('StatusCode 300: Cuando no encuentra un artista con el mbid ingresado.', async () => {
        await getService().perfilArtistNumberOne(req, res);
        expect(res.statusCode).to.be.equal(300);
    });

    let getService = () => {
        return proxyquire('../controller/perfil_artist_number_one', {});
    };
});

describe('Escenario ClientePerfilTrack StatusCode 500', async () => {
    beforeEach(async () => {
        req = {
        };
        res = new Response();
    });

    it('StatusCode 500: Cuando se produce un error en el servidor.', async () => {
        await getService().perfilArtistNumberOne(req, res);
        expect(res.statusCode).to.be.equal(500);
    });

    let getService = (opciones) => {
        return proxyquire('../controller/perfil_artist_number_one', {});
    };
});