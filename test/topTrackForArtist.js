const
    expect = require('chai').expect,
    proxyquire = require('proxyquire'),
    Response = require('mock-express-response');

let req, res;

describe('Escenario TopTenArtists StatusCode 200', async () => {
    beforeEach(async () => {
        req = {};
        res = new Response();
    });

    it('StatusCode 200: Cuando despliega correctamente el Top Ten Track de un artista.', async () => {
        await getService().topTrackForArtistNumberOne(req, res);
        expect(res.statusCode).to.be.equal(200);
    });

    let getService = () => {
        return proxyquire('../controller/top_track_for_artist_number_one', {});
    };
});

describe('Escenario TrackForArtist StatusCode 500', async () => {
    beforeEach(async () => {
        req = {};
        res = new Response();
    });

    it('StatusCode 500: Cuando se produce un error en la respuesta del servicio clientTopArtist.', async () => {
        await getService().topTrackForArtistNumberOne(req, res);
        expect(res.statusCode).to.be.equal(500);
    });

    let getService = () => {
        return proxyquire('../controller/top_track_for_artist_number_one', {
            '../client/clientTopArtist': {
                clientTopArtist(req, res){
                    process.nextTick(function () {
                        return '';
                    })
                }
            },
            'config': {
                config_server: {
                    address: 'http://localhost:80',
                    profile: 'local'
                }

            }
        });
    };
});

describe('Escenario TrackForArtist StatusCode 500', async () => {
    beforeEach(async () => {
        req = {};
        res = new Response();
    });

    it('StatusCode 500: Cuando se produce un error en la respuesta del servicio clientTopTrack.', async () => {
        await getService().topTrackForArtistNumberOne(req, res);
        expect(res.statusCode).to.be.equal(500);
    });

    let getService = () => {
        return proxyquire('../controller/top_track_for_artist_number_one', {
            '../client/clientTopTrack': {
                clientTopTrack(req, res){
                    process.nextTick(function () {
                        return '';
                    })
                }
            },
            'config': {
                config_server: {
                    address: 'http://localhost:80',
                    profile: 'local'
                }

            }
        });
    };
});