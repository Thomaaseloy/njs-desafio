var clientTopArtist = require('../client/clientTopArtist');
var clientTopTrack = require('../client/clientTopTrack');

function getTopTenTracksArtist(tracks, mbidArtist) {
    var returnTracks = [];
    tracks.find(data => {
        if(data.artist.mbid === mbidArtist){
            returnTracks.push(data);
        }
    })
        
    return getTopTenTracks(returnTracks);
}

function getTopTenTracks(tracks){
    var returnTracks = [];
    if(tracks.length > 0){
        for (let i = 0; i < 10; i++) {
            returnTracks.push(tracks[i]);
        }
    }
    return returnTracks;
}

function getPerfilArtist(artists, mbid){
    var artist;
    artist = artists.find(data => data.mbid === mbid); 
    return artist;
}

async function perfilArtistNumberOne(req,res) {
    try{
        var mbid = req.headers.mbid;
        var callclientTopArtist = await clientTopArtist(req,res);
        var callclientTopTrack= await clientTopTrack(req,res);
        var perfilArtist = getPerfilArtist(callclientTopArtist.data.artists.artist,mbid);
        if(perfilArtist !== undefined){
            var topTenTracksArtist = getTopTenTracksArtist(callclientTopTrack.data.toptracks.track,mbid);
            perfilArtist.toptracks = topTenTracksArtist;
            return {data:perfilArtist, statusCode:200};
        } else {
            res.statusCode = 300;
            res.json({ message: 'Artista No Encontrado' });    
        }
    }catch(e){
        res.statusCode = 500;
        res.json({ message: 'Error Interno' });
    }  
}

exports.perfilArtistNumberOne = perfilArtistNumberOne;