var clientTopArtist = require('../client/clientTopArtist');
var clientTopTrack = require('../client/clientTopTrack');

function getTopTenSongs(tracks, mbidArtist) {
    var returnTracks = [];
    tracks.find(function (data) {
        if (parseInt(data['@attr'].rank) <= 10 && data.artist.mbid === mbidArtist) {
            returnTracks.push(data);
        }
    })
    return returnTracks;
}

async function topTrackForArtistNumberOne(req,res) {
    try{
        var callclientTopArtist = await clientTopArtist(req,res);
        var callclientTopTrack= await clientTopTrack(req,res);
        var topTenSongs = getTopTenSongs(callclientTopTrack.data.toptracks.track,callclientTopArtist.data.artists.artist[0].mbid);
        return {data:topTenSongs, statusCode:200};
    }catch(e){
        res.statusCode = 500;
        res.json({ message: 'Error Interno' });
    }  
}

exports.topTrackForArtistNumberOne = topTrackForArtistNumberOne ;