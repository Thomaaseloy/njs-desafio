var clientTopArtist = require('../client/clientTopArtist');

function getTopTenArtists(artists) {
    var returnArtists = [];
    for (let i = 0; i < 10; i++) {
        returnArtists.push(artists[i]);
    }
    return returnArtists;
}

async function topArtist(req,res) {
    try{
        var callclientTopArtist = await clientTopArtist(req,res);
        var artists = getTopTenArtists(callclientTopArtist.data.artists.artist);
        return {data:artists, statusCode:200};
    }catch(e){
        res.statusCode = 500;
        res.json({ message: 'Error Interno' });
    }  
}

exports.topArtist = topArtist;